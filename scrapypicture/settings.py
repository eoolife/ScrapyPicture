# Scrapy settings for pict project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'pict'

SPIDER_MODULES = ['pict.spiders']
NEWSPIDER_MODULE = 'pict.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'pict (+http://www.yourdomain.com)'
ITEM_PIPELINES = {
    'pict.pipelines.PictPipeline': 300,
}

def setup_django_environment(path):
    import imp, os, sys
    from django.core.management import setup_environ
    m = imp.load_module('settings', *imp.find_module('settings', [path]))
    setup_environ(m)
    sys.path.append(os.path.abspath(os.path.join(path, os.path.pardir)))

setup_django_environment("/home/sonic/workplace/webproject/project/pictcms/pictcms")

